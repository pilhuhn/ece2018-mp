/*
 * Copyright 2018 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.bsd.ece2018.frontend;

import io.opentracing.Scope;
import io.opentracing.Span;
import io.opentracing.Tracer;
import io.smallrye.opentracing.SmallRyeClientTracingFeature;
import java.net.URL;
import java.time.temporal.ChronoUnit;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.faulttolerance.Fallback;
import org.eclipse.microprofile.faulttolerance.Retry;
import org.eclipse.microprofile.faulttolerance.Timeout;
import org.eclipse.microprofile.rest.client.RestClientBuilder;

/**
 * @author hrupp
 */
public class BackendService {

  @Inject
  @ConfigProperty(name = "de.bsd.ece2018.frontend.BackendService/mp-rest/url")
  private URL backendServiceUrl;

  @Inject
  private Tracer tracer;

  @Fallback(fallbackMethod = "fallback")
  @Timeout(value = 200L, unit = ChronoUnit.MILLIS)
  @Retry(delay = 100L, maxRetries = 5)
  public String getGreeting(String mode, Span span) {
    Client client = createClient(tracer, backendServiceUrl);

    // span has to be activated because fault tolerance is executed in a different thread
    // It will be handled automatically in the next MP versions.
    // Once the issue is resolved manual span propagation can be removed.
    // https://github.com/eclipse/microprofile-opentracing/issues/108
    Response response = null;
    try (Scope scope = tracer.scopeManager().activate(span, false)) {
      mode = mode.toLowerCase();
      if (mode.contains("l")) {
        response = client.getFailure();
      } else if (mode.contains("ra")) {
        response = client.getRandom();
      } else {
        response = client.getOk();
      }
      JsonObject bla = response.readEntity(JsonObject.class);
      return bla.getString("msg");
    } finally {
      if (response != null) {
        response.close();
      }
    }
  }

  @Path("/")
  @Produces(MediaType.APPLICATION_JSON)
  public interface Client {

    @GET
    @Path("/ok")
    Response getOk();

    @GET
    @Path("/fail")
    Response getFailure();

    @GET
    @Path("/random")
    Response getRandom();
  }

  private String fallback(String mode, Span span) {
    return "@@ fallback @@";
  }

  private static Client createClient(Tracer tracer, URL backendServiceUrl) {
    return RestClientBuilder.newBuilder()
        .baseUrl(backendServiceUrl)
        // We have to create client "manually" because tracing is not supported
        // at MP spec level. This workaround works only in Thorntail.
        // Once it is supported client will be injected as CDI bean without any additional configuration.
        // https://github.com/eclipse/microprofile-opentracing/issues/82
        .register(new SmallRyeClientTracingFeature(tracer))
        .build(Client.class);
  }
}
