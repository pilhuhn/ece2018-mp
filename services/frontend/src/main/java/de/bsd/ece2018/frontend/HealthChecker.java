/*
 * Copyright 2018 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.bsd.ece2018.frontend;

import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;

/**
 * @author hrupp
 */
@Health
public class HealthChecker implements HealthCheck {
  @Override
  public HealthCheckResponse call() {

    HealthCheckResponseBuilder rb = HealthCheckResponse.builder()
        .name("Check12")
        .withData("hilfe-url","http:/..../health");

    if (Math.random() < 0.5) {
      rb.down();
    } else {
      rb.up();
    }

    return rb.build();
  }
}
