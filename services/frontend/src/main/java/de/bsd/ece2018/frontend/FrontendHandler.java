/*
 * Copyright 2018 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.bsd.ece2018.frontend;

import io.opentracing.Scope;
import io.opentracing.Tracer;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.metrics.Counter;
import org.eclipse.microprofile.metrics.annotation.Metric;
import org.eclipse.microprofile.metrics.annotation.Timed;

/**
 * @author hrupp
 */
@RequestScoped
@Path("/")
public class FrontendHandler {

  @Inject
  @ConfigProperty(name = "greeting", defaultValue = "Grüezi")
  String greeting;

  @Inject
  Tracer tracer;

  @Inject
  BackendService backendService;

  @Inject
  WorkerBean bean;

  @Inject
  @Metric(absolute = true)
  Counter callCount;

  @GET
  @Timed
  public Response doSomething(@QueryParam("mode") @DefaultValue("ok") String mode) {
    tracer.activeSpan().setBaggageItem("mode", mode);
/*
    try {
      Thread.sleep(5000);
    } catch (InterruptedException e) {
      e.printStackTrace();  // TODO: Customise this generated block
    }
*/

    callCount.inc();

    bean.doWork();

    String msg = backendService.getGreeting(mode, tracer.activeSpan());
    return Response.ok(greeting + "[" + callCount.getCount() + "] : " + msg + "\n").build();
  }

}
