#!/usr/bin/env bash
set -x

mvn clean install -Pthorntail
java -jar target/frontend-thorntail.jar
