#!/usr/bin/env bash
set -x

mvn clean install -Pthorntail
java -Dswarm.port.offset=1000 -jar target/backend-thorntail.jar
