/*
 * Copyright 2018 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.bsd.ece2018.backend;


import io.opentracing.Tracer;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.metrics.Counter;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.Metric;

/**
 * @author hrupp
 */
@SuppressWarnings("unused")
@RequestScoped
public class BackendHandler implements BackendService {

  @Inject
  Tracer tracer;

  @Inject
  @Metric(name="randomOk", absolute = true)
  Counter randomOk;

  @Inject
  @Metric(name="randomFail", absolute = true)
  Counter randomFail;

  public Response getFailure() {
    return Response.serverError()
        .entity(toJson("Uh oh, something went wrong"))
        .build();
  }

  public Response getOk() {
    System.out.println(tracer.activeSpan().getBaggageItem("mode"));
    return Response.ok(toJson("Yay!"))
        .build();
  }

  @Override
  @Counted(monotonic = true, absolute = true)
  public Response getRandom() {

    Response.ResponseBuilder builder;

    double probability = Math.random();
    tracer.activeSpan().setTag("probability", probability);
    if (probability < 0.50) {
      builder = Response.ok(toJson("Gut"));
      randomOk.inc();
    }
    else {
      builder = Response.serverError()
          .entity(toJson("Nicht gut"));
      randomFail.inc();
    }

    return builder.build();
  }

  private JsonObject toJson(String msg) {
    JsonObject jo = Json.createObjectBuilder().add("msg",msg).build();
    return jo;
  }
}
