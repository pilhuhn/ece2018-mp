# Thorntail MicroProfile Example


To create request run the following command. It can be modified with `?mode=random|ok|fail` query parameter.
`curl  localhost:8080`

## OpenTracing

MicroProfile-OpenTracing is a distributed tracing integration for MicroProfile and Java EE
technologies. It defines instrumentation semantics and exposes OpenTracing tracer to the application.

* [Specification](https://github.com/eclipse/microprofile-opentracing)
* [OpenTracing](https://opentracing.io/)

### Start Jaeger server
`docker run --rm -it -p 16686:16686 -p 14268:14268 -p 6831:6831/udp -p 5778:5778 jaegertracing/all-in-one:latest`

Or run a binary from [jaegertracing.io](https://www.jaegertracing.io/)

### Steps
1. Configure tracing by adding Jaeger properties to project-defaults.yml.
2. Add `@Traced` to `WorkerBean`.
3. Inject tracer to `BackendHandler` and add probability as a tag to the current active span.
4. Add query parameter mode to baggage in `FrontendHandler` and log mode to stdout in `BackendHandler`.
